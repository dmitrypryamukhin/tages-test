import { ref, watch } from 'vue'
import { defineStore } from 'pinia'

const STORE_NAME = 'shopSettings'

export const useShopSettingsStore = defineStore(STORE_NAME, () => {
  const settings = ref({
    sort: 0,
    material: 0
  })

  const storage = localStorage.getItem('settings')

  if (storage) {
    settings.value = JSON.parse(storage)._value
  }

  function setSort(val) {
    settings.value.sort = val
  }
  function setMaterial(val) {
    settings.value.material = val
  }

  watch(
    () => settings,
    (state) => {
      localStorage.setItem('settings', JSON.stringify(state))
    },
    { deep: true }
  )

  return { settings, setSort, setMaterial }
})
