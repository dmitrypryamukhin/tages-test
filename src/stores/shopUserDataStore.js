import { ref, watch } from 'vue'
import { defineStore } from 'pinia'

const STORE_NAME = 'shopUserData'

export const useShopUserDataStore = defineStore(STORE_NAME, () => {
  const userData = ref({
    likedProducts: [],
    productsInCart: []
  })

  const storage = localStorage.getItem('userData')
  if (storage) {
    userData.value = JSON.parse(storage)._value
  }

  function toggleToCart(id) {
    let index = userData.value.productsInCart.findIndex((item) => item == id)
    if (index == -1) {
      userData.value.productsInCart.push(id)
    } else {
      userData.value.productsInCart.splice(index, 1)
    }
  }

  function toggleLikedProduct(id) {
    let index = userData.value.likedProducts.findIndex((item) => item == id)
    if (index == -1) {
      userData.value.likedProducts.push(id)
    } else {
      userData.value.likedProducts.splice(index, 1)
    }
  }

  watch(
    () => userData,
    (state) => {
      localStorage.setItem('userData', JSON.stringify(state))
    },
    { deep: true }
  )

  return { userData, toggleToCart, toggleLikedProduct }
})
