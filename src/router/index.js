import { createRouter, createWebHistory } from 'vue-router'
import ShopPage from '../pages/ShopPage.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'shop',
      component: ShopPage
    }
  ]
})

export default router
