import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: [{ find: '@', replacement: path.resolve(__dirname, 'src') }]
    //   alias: {
    //     '@': path.resolve(__dirname, './src'),
    //     // '@': fileURLToPath(new URL('./src', import.meta.url))
    //   }
  }
})
